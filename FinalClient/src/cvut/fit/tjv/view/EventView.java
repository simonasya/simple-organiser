package cvut.fit.tjv.view;
import cvut.fit.tjv.entities.Event;
import cvut.fit.tjv.entities.Information;
import cvut.fit.tjv.rest.EventClient;
import cvut.fit.tjv.rest.InformationClient;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class EventView {
    EventClient cl = new EventClient();
    InformationClient ic = new InformationClient();
    
    public EventView(){}
    
    public void printEvent(String id){
        Event event = cl.find_JSON(Event.class, id);
        System.out.println("Name: " + event.getName()+ "\nTime: " + event.getTimeOfEvent());
        
        Information information = event.getInformationid();
        if(information != null){
            System.out.println("Program: " + information.getProgram() + "\nNote: " + information.getDetails());
        }
        System.out.println();
    }
    public void printList(){
        System.out.println("\nEVENTS: ");
        
        String s = cl.findAll_JSON(String.class);        
        JSONParser parser = new JSONParser();
                
        try{
            Object obj = parser.parse(s);
            JSONArray array = (JSONArray)obj;
            
            for(int i = 0; i < Integer.parseInt(cl.countREST()); i++){
                Object element = parser.parse(array.get(i).toString());
                JSONObject objElement = (JSONObject)element;
                System.out.print(objElement.get("id") + " ");
                System.out.println(objElement.get("name"));
            }
            System.out.println();
        }
        catch(Exception e){
            System.out.println(e.toString()); 
        }
    }
}
