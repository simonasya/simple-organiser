package cvut.fit.tjv.view;
import cvut.fit.tjv.entities.Note;
import cvut.fit.tjv.rest.NoteClient;
import java.util.List;
import java.lang.Object;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class NoteView {
    NoteClient cl = new NoteClient();
    
    public NoteView(){}
    
    public void printNote(String id){
        Note note = cl.find_JSON(Note.class, id);
        System.out.println("Title: " + note.getTitle());
        System.out.println("---------------");
        System.out.println(note.getText() + "\n");
    }
    public void printList(){
        System.out.println();
        System.out.println("NOTES: ");
        
        String s = cl.findAll_JSON(String.class);        
        JSONParser parser = new JSONParser();
                
        try{
            Object obj = parser.parse(s);
            JSONArray array = (JSONArray)obj;
            
            for(int i = 0; i < Integer.parseInt(cl.countREST()); i++){
                Object element = parser.parse(array.get(i).toString());
                JSONObject objElement = (JSONObject)element;
                System.out.print(objElement.get("id") + " ");
                System.out.println(objElement.get("title"));
            }
            System.out.println();
        }
        catch(Exception e){
            System.out.println(e.toString()); 
        }
    }   
}
