package cvut.fit.tjv.client;
import cvut.fit.tjv.view.*;
import cvut.fit.tjv.entities.*;
import cvut.fit.tjv.rest.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Scanner;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class Main {

    public static void main(String[] args) {
        while(menu()){};    
    }
    static Boolean menu(){
        System.out.println("MENU:");
        System.out.println("1 --> Add new note.");
        System.out.println("2 --> Show list of notes");
        System.out.println("3 --> Show note");
        System.out.println("4 --> Edit existing note");
        
        System.out.println("5 --> Delete existing note");
        System.out.println("6 --> Add new event");
        System.out.println("7 --> Show list of events");
        System.out.println("8 --> Show event");
        System.out.println("9 --> Edit existing event");
        System.out.println("10 --> Delete existing event");
        System.out.println("11 --> Exit");
        
        int choice;
        System.out.print("Your choice: ");
        Scanner scanner = new Scanner(System.in);
        choice = scanner.nextInt();

        switch (choice) {
            case 1: addNote(); 
                break;
            case 2: showNoteList(); 
                break;
            case 3: showNote(); 
                break;
            case 4: editNote(); 
                break;
            case 5: deleteNote();
                break;
            case 6: addEvent();
                break;
            case 7: showEventList();
                break;
            case 8: showEvent();
                break;
            case 9: editEvent();
                break;
            case 10: deleteEvent();
                break;
            case 11: return false;
        }
        
        return true;
    }

    private static void addNote() {
        NoteClient cl = new NoteClient();
        Note n = new Note();
        Scanner sc = new Scanner(System.in);
        
        System.out.print("\nTitle: ");
        n.setTitle(sc.nextLine());
        System.out.println("Text: ");
        n.setText(sc.nextLine());
        
        cl.create_JSON(n);
        System.out.println("----> ADDED\n");
    }

    private static void showNoteList() {
        NoteView nv = new NoteView();
        nv.printList();
    }

    private static void showNote() {
        NoteView nv = new NoteView();
        Scanner sc = new Scanner(System.in);
        System.out.print("\nID: ");
        nv.printNote(sc.next());
    }

    private static void editNote() {
        NoteClient cl = new NoteClient();
        NoteView nv = new NoteView();
        Scanner scanner = new Scanner(System.in);
        
        System.out.print("\nID: ");
        String id = scanner.nextLine();
        Note note = cl.find_JSON(Note.class, id);
        nv.printNote(id);
 
        System.out.print("New Title: ");
        note.setTitle(scanner.nextLine());
        System.out.println("New Text: ");
        note.setText(scanner.nextLine());
        
        cl.edit_JSON(note, note.getId().toString());
        System.out.println("----> EDITED\n"); 
    }
    
    private static void deleteNote() {
        NoteClient cl = new NoteClient();
        Scanner sc = new Scanner(System.in);
        System.out.print("ID: ");
        cl.remove(sc.next());
        System.out.println("----> DELETED\n");
    }

    private static void addEvent() {
        Information info = new Information();
        InformationClient ic = new InformationClient();
        EventClient ec = new EventClient();
        Event event = new Event();
        Scanner sc = new Scanner(System.in);
        
        System.out.print("\nName of event: ");
        event.setName(sc.nextLine());
        System.out.print("Time of event (YYYY-MM-DD HH:MM:SS): ");
        String dateFormat = "YYYY-MM-DD HH:MM:SS";
        try{
            event.setTimeOfEvent(new SimpleDateFormat(dateFormat).parse(sc.nextLine()));
        } 
        catch(ParseException ex){
            System.out.println(ex.toString());
        }
        
        System.out.print("Add details? (y/n):");
        if(!"y".equals(sc.nextLine())){
             ec.create_JSON(event);
             System.out.println("----> ADDED\n");
             return;
        }
            
        info = setInformation(info);
        String infoID = getInfoID(info);
        event.setInformationid(ic.find_JSON(Information.class, infoID));
        
        ec.create_JSON(event);
        System.out.println("----> ADDED\n");
        
    }
    
    private static String getInfoID(Information info){
        InformationClient ic = new InformationClient();
        ic.create_JSON(info);
        String s = ic.findAll_JSON(String.class);        
        JSONParser parser = new JSONParser();
        String infoID = null;
        try{
            Object obj = parser.parse(s);
            JSONArray array = (JSONArray)obj;
            int lastIndex = Integer.parseInt(ic.countREST()) - 1;
            Object element = parser.parse(array.get(lastIndex).toString());
            JSONObject objElement = (JSONObject)element;
            infoID = objElement.get("id").toString();
        }
        catch (Exception ex){
            System.out.println(ex.toString());
        }
        return infoID;
    }

    private static void showEventList() {
        EventView ev = new EventView();
        ev.printList();
    }

    private static void showEvent() {
        EventView ev = new EventView();
        Scanner sc = new Scanner(System.in);
        System.out.print("\nID: ");
        ev.printEvent(sc.next());
    }

    private static void editEvent() {
        InformationClient ic = new InformationClient();
        EventClient ec = new EventClient();
        EventView ev = new EventView();
        Scanner sc = new Scanner(System.in);
        
        System.out.print("ID: ");
        String id = sc.nextLine();
        ev.printEvent(id);
        Event event = ec.find_JSON(Event.class, id);
        Information info = event.getInformationid();
        
        System.out.print("New name of event: ");
        event.setName(sc.nextLine());
        System.out.print("New time of event (YYYY-MM-DD HH:MM:SS): ");
        String dateFormat = "YYYY-MM-DD HH:MM:SS";
        try{
            event.setTimeOfEvent(new SimpleDateFormat(dateFormat).parse(sc.nextLine()));
        } 
        catch(ParseException ex){
            System.out.println(ex.toString());
        }
        
        System.out.print("Edit details? (y/n):");
        if(!"y".equals(sc.nextLine())){
             ec.edit_JSON(event, id);
             System.out.println("----> EDITED\n");
             return;
        }
        
        String infoID;
        if(info == null){
            info = setInformation(new Information());
            infoID = getInfoID(info);
            event.setInformationid(ic.find_JSON(Information.class, infoID));
            ec.edit_JSON(event, id);
        }
        else{
            System.out.print("New Program: \n");
            info.setProgram(sc.nextLine());
            System.out.print("New Note: \n");
            info.setDetails(sc.nextLine());
            ec.edit_JSON(event, id);
            ic.edit_JSON(info, info.getId().toString());
        }
        System.out.println("----> EDITED\n");
    }

    private static void deleteEvent() {
        EventClient cl = new EventClient();
        InformationClient ic = new InformationClient();
        Scanner sc = new Scanner(System.in);
        
        System.out.print("ID: ");
        String id = sc.next();
        
        if(cl.find_JSON(Event.class, id) == null)
            return;
        String infoID = cl.find_JSON(Event.class, id).getInformationid().getId().toString();
        cl.find_JSON(Event.class, id).setInformationid(null);
        cl.remove(id);
        if(infoID != null)
            ic.remove(infoID);
        System.out.println("----> DELETED\n");
    }

    private static Information setInformation(Information info) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Program: \n");
        info.setProgram(sc.nextLine());
        System.out.print("Note: \n");
        info.setDetails(sc.nextLine());
        return info;
    }
    
}
