package cvut.fit.tjv.entities;

import cvut.fit.tjv.entities.Event;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2016-12-28T21:51:47")
@StaticMetamodel(Information.class)
public class Information_ { 

    public static volatile CollectionAttribute<Information, Event> eventCollection;
    public static volatile SingularAttribute<Information, String> details;
    public static volatile SingularAttribute<Information, Integer> id;
    public static volatile SingularAttribute<Information, String> program;

}