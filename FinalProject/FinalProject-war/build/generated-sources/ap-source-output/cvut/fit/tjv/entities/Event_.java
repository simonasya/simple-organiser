package cvut.fit.tjv.entities;

import cvut.fit.tjv.entities.Information;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2016-12-28T21:51:47")
@StaticMetamodel(Event.class)
public class Event_ { 

    public static volatile SingularAttribute<Event, String> name;
    public static volatile SingularAttribute<Event, Information> informationid;
    public static volatile SingularAttribute<Event, Integer> id;
    public static volatile SingularAttribute<Event, Date> timeOfEvent;

}