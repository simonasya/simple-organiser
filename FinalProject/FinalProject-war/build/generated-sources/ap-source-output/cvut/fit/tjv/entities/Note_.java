package cvut.fit.tjv.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2016-12-28T21:51:47")
@StaticMetamodel(Note.class)
public class Note_ { 

    public static volatile SingularAttribute<Note, Integer> id;
    public static volatile SingularAttribute<Note, String> text;
    public static volatile SingularAttribute<Note, String> title;

}